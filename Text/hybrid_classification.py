import joblib
import pandas as pd
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, VotingClassifier, AdaBoostClassifier
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import cross_val_score, StratifiedKFold
from sklearn.naive_bayes import ComplementNB
from sklearn.pipeline import Pipeline
from Text import textprocessing

# assuming the dataset is in a CSV file
df = pd.read_excel("Data\\comments.xlsx")
df['Comment'] = df['Comment'].apply(str)

# define the pipeline for text classification
text_clf1 = Pipeline([
    ('vect', CountVectorizer(analyzer=textprocessing.processText)),
    ('tfidf', TfidfTransformer(use_idf=True)),
    ("clf", ComplementNB()),
])

# define the pipeline for text classification
text_clf2 = Pipeline([
    ('vect', CountVectorizer(analyzer=textprocessing.processText)),
    ('tfidf', TfidfTransformer(use_idf=True)),
    ("clf", RandomForestClassifier()),
])

# define the pipeline for text classification
text_clf3 = Pipeline([
    ('vect', CountVectorizer(analyzer=textprocessing.processText)),
    ('tfidf', TfidfTransformer(use_idf=True)),
    ("clf", AdaBoostClassifier()),
])

# define the pipeline for text classification
text_clf4 = Pipeline([
    ('vect', CountVectorizer(analyzer=textprocessing.processText)),
    ('tfidf', TfidfTransformer(use_idf=True)),
    ("clf", GradientBoostingClassifier()),
])

# define the voting classifier
voting_clf = VotingClassifier(estimators=[('nb', text_clf1), ('rf', text_clf2), ('ad', text_clf3), ('gb', text_clf4)],
                              voting='soft')
# train the model on the training set
voting_clf.fit(df["Comment"], df["Spam"])


def predict_spam_probability(new_comment):
    # Predict the spam probability for a new comment
    spam_prob = voting_clf.predict_proba(new_comment['Comment'])
    print(spam_prob)
    return spam_prob


def outputMetrics():
    values = range(43)
    # iterate from i = 0 to i = 3
    for i in values:

        # Define the k-fold cross validation
        cv = StratifiedKFold(n_splits=10, shuffle=True, random_state=i)

        # Perform cross validation on the classifier
        scores = cross_val_score(voting_clf, df["Comment"], df["Spam"], cv=cv)
        # Calculate the AUC-ROC score
        auc_roc = cross_val_score(voting_clf, df["Comment"], df["Spam"], cv=cv, scoring="roc_auc").mean()

        # Print the cross-validation results and AUC-ROC score for each random state
        print("Random State %d: Accuracy: %0.2f( +/- %0.2f)  AUC-ROC: %0.2f" % (
        i, scores.mean(), scores.std() * 2, auc_roc))