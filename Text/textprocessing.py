import re
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from nltk.corpus import wordnet
import string
import unicodedata
import requests

nltk.download("stopwords")
nltk.download("punkt")
nltk.download("wordnet")
nltk.download('averaged_perceptron_tagger')

stopwordEn = stopwords.words("english")
stemmer = PorterStemmer()


def lemmaWord(word):
    lemma = wordnet.morphy(word)
    if lemma is not None:
        return lemma
    else:
        return word


def stemWord(word):
    stem = stemmer.stem(word)
    if stem is not None:
        return stem
    else:
        return word


def translateText(text, target_lang):
    api_key = 'YOUR_API_KEY'
    endpoint = f'https://api.cognitive.microsofttranslator.com/translate?api-version=3.0&to={target_lang}'
    headers = {'Ocp-Apim-Subscription-Key': api_key, 'Content-type': 'application/json'}
    body = [{'text': text}]
    response = requests.post(endpoint, headers=headers, json=body)
    if response.status_code != 200:
        raise ValueError("Failed to translate text")
    translation = response.json()[0]['translations'][0]['text']
    return translation


def analyseEmojis(text):
    spam_emojis = ['🎖️', '💸', '🤑', '📈', '📊', '📉', '🚀', '🎁']
    spam_count = sum([1 for c in text if c in spam_emojis])
    return spam_count


def processText(text, lemma=False, gram=1, rmStop=True, rmEmoji=True, pos=False, lang='en'):
    text = re.sub(r"(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b", "", text, flags=re.MULTILINE)

    if lang != 'en':
        text = translateText(text, 'en')

    spam_emoji_count = analyseEmojis(text)

    if rmEmoji:
        text = re.sub(r'[^\w\s' + string.punctuation + ']', '', text, flags=re.UNICODE)

    tokens = word_tokenize(text)
    new_tokens = []
    stoplist = stopwordEn if rmStop else []
    for i in tokens:
        i = i.lower()
        if i.isalpha() and (i not in stoplist):
            if lemma:
                i = lemmaWord(i)
            if pos:
                pos_tag = nltk.pos_tag([i])[0][1]
                new_tokens.append(i + '/' + pos_tag)
            else:
                new_tokens.append(i)

    if spam_emoji_count > 0:
        new_tokens.append('spam_emoji:' + str(spam_emoji_count))

    if gram <= 1:
        return new_tokens
        print(new_tokens)
    else:
        print([" ".join(i) for i in nltk.ngrams(new_tokens, gram)])
        return [" ".join(i) for i in nltk.ngrams(new_tokens, gram)]