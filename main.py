import tkinter as tk
from tkinter import messagebox
import customtkinter
import pandas as pd
from Text import hybrid_classification

# System Settings
customtkinter.set_appearance_mode("dark")
customtkinter.set_default_color_theme("green")

# Frame
app = customtkinter.CTk()
app.geometry("512x512")
app.title("Spam Detection: Comment Mode")

# Adding UI Elements
title = customtkinter.CTkLabel(app, text="Spam Detection: Instagram Comment")
title.pack(padx=10, pady=10)

# Comment Input
comment_var = tk.StringVar()
comment = customtkinter.CTkEntry(app, placeholder_text="CTkEntry", width=350, height=100,
                                 textvariable=comment_var, border_width=2, corner_radius=10, border_color="white")
comment.pack(fill="both", padx=10, pady=10, expand=False)
comment.configure(justify="left")


# Function to validate input
def validate_input():
    input_text = comment_var.get()

    # Remove leading/trailing spaces and convert to lowercase
    input_text = input_text.strip().lower()

    # Remove dashes
    input_text = input_text.replace("-", "")

    # Check if input exceeds 2000 characters
    if len(input_text) > 2000:
        messagebox.showerror("Error", "Input cannot exceed 2000 characters.")
        return False

    # Set the validated input to the entry box variable
    comment_var.set(input_text)
    return True


# Function to get spam probability
def get_spam_probability():
    try:
        # Validate input before making predictions
        if not validate_input():
            return

        # make predictions on new data
        average_spam_prob = hybrid_classification.predict_spam_probability(
            pd.DataFrame({"Comment": [comment_var.get()]}))
        print(f"Average Spam probability: {average_spam_prob[0][1]:.2f}")
        output_spam_probability(average_spam_prob[0][1])

    except Exception as e:
        messagebox.showerror("Error", f"An error occurred: {str(e)}")


# Function to output stuff
def output_spam_probability(average_spam_prob):
    average_spam_prob_percentage = average_spam_prob * 100

    if average_spam_prob <= 0.5:
        spam_label.configure(state="normal", text_color="Green",
                             text=f"This comment is unlikely to be spam")

    elif 0.5 < average_spam_prob < 0.8:
        spam_label.configure(state="normal", text_color="Yellow",
                             text=f"This comment is likely to be spam")

    elif 0.8 < average_spam_prob < 1:
        spam_label.configure(state="normal", text_color="Red",
                             text=f"This comment is very likely to be spam")

    else:
        spam_label.configure(state="disabled", fg_color='transparent', text=f"")
        messagebox.showerror("Title", "Message")


# Button to get spam probability
button = customtkinter.CTkButton(
    app, text="Check Spam Probability", command=get_spam_probability
)
button.pack(pady=10)

# Label to display spam probability
spam_label = customtkinter.CTkLabel(app)
spam_label.configure(state="disabled", fg_color='transparent', text=f"")
spam_label.pack()

# Run App
app.mainloop()
