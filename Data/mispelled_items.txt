original,misspelled
accommodate,accomodate
achieve,acheive
across,accross
address,addres
alcohol,alchohol
all right,alright
almost,almoust
already,allready
although,altho
always,alwas
amateur,amature
analyze,analize
and,an
angel,angle
anniversary,aniversary
apparent,apparant
appreciate,apreciate
appropriate,aproppriate
approximately,aproximately
architect,archtect
argument,arguement
as,ass
assistance,assisstance
athletic,atheltic
attendance,attendence
beautiful,beautifull
beginning,begining
believe,beleive
benefit,benifit
between,betweenn
biology,bilogy
boundary,bouundary
business,buisness
calendar,calender
camouflage,camoflage
campaign,campain
category,catagory
cemetery,cemetary
characteristic,characteristc
colleague,collegue
coming,comming
committee,commitee
communication,communcation
completely,completly
concede,conceed
congratulations,congradulations
conscious,concious
consensus,concensus
consequence,consequense
considerable,consideral
consistently,consistantly
controversy,controversey
correspondence,correspondance
criticism,critisism
curiosity,curiousity
definitely,definately
definition,defenition
describe,discribe
description,desciption
desperate,desparate
deteriorate,deteriate
development,developement
difference,differance
difficulty,dificulty
disappear,disapear
disappoint,disapoint
disastrous,disasterous
dissatisfied,disatisfied
distinguish,distingish
eighth,eigth
eligible,eligable
embarrass,embarass
emergency,emergancy
environment,environent
equivalent,equivilent
especially,especally
exaggerate,exagerate
excellent,exellent
existence,existance
experience,experiance
explanation,explaination
familiar,familar
finally,finaly
fluorescent,flourescent
foreign,foriegn
forward,forwad
friendship,frendship
furthermore,futhermore
generally,generalyy
government,goverment
grammar,grammer
guarantee,garantee
happened,happend
harassment,harrassment
height,heigth
history,histroy
honorable,honourable
hoping,hopfully
humorous,humourous
immediately,immediatly
impression,impressionable
incidentally,incidently
independent,independant
individual,individiual
inquiry,enquiry
intelligence,inteligence
interest,intrest
irrelevant,irrelevent
knowledgeable,knowledgable
laboratory,laboritory
language,langauge
liaison,liason
library,libary
licensee,licentiate
literate,litterate
maintenance,maintainance
manager,mananger
maneuver,manouver
marriage,marraige
medieval,medeval
memoir,memior