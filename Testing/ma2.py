import re

import nltk
import pandas as pd
from nltk.corpus import stopwords
from nltk.corpus import wordnet
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize

nltk.download("stopwords")
nltk.download("punkt")
nltk.download("wordnet")

stopwordEn = stopwords.words("english")
stemmer = PorterStemmer()


def lemmaWord(word):
    lemma = wordnet.morphy(word)
    if lemma is not None:
        return lemma
    else:
        return word


def stemWord(word):
    stem = stemmer.stem(word)
    if stem is not None:
        return stem
    else:
        return word


def processText(text, lemma=False, gram=1, rmStop=True, rmEmoji=True, rmPunc=True, rmFont=True):
    # remove URLs and erroneous symbols
    text = re.sub(r"(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b", "", text, flags=re.MULTILINE)

    # tokenize text and remove stop words
    tokens = word_tokenize(text)
    new_tokens = []
    stoplist = stopwordEn if rmStop else []
    for i in tokens:
        i = i.lower()
        if i.isalpha() and (i not in stoplist):
            if lemma:
                i = lemmaWord(i)
            new_tokens.append(i)

    # generate n-grams if gram > 1
    if gram <= 1:
        return new_tokens
    else:
        return [" ".join(i) for i in nltk.ngrams(new_tokens, gram)]


def getTags(text):
    token = word_tokenize(text)
    token = [i.lower() for i in token]
    train_tags = nltk.pos_tag(token)

# Import necessary libraries
from sklearn.ensemble import RandomForestClassifier, BaggingClassifier, AdaBoostClassifier, GradientBoostingClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.preprocessing import StandardScaler

# Load the dataset
df = pd.read_excel("comments.xlsx")
df['Comment'] = df['Comment'].apply(str)

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(df['Comment'], df['Spam'], test_size=0.2, random_state=42)
# Define the pipeline for text classification
classifiers = [('RandomForest', RandomForestClassifier()),
               ('Bagging', BaggingClassifier()),
               ('AdaBoost', AdaBoostClassifier()),
               ('GradientBoosting', GradientBoostingClassifier())]

# Train and evaluate each classifier
for clf_name, clf in classifiers:
    text_clf = Pipeline([
        ('vect', CountVectorizer(analyzer=processText)),
        ('tfidf', TfidfTransformer(use_idf=True)),
        ('scaler', StandardScaler(with_mean=False)),
        ("clf", clf),
    ])
    print(f'Training {clf_name}...')
    text_clf.fit(X_train, y_train)

    # Evaluate the model on the testing set
    y_pred = text_clf.predict(X_test)  # convert to dense matrix
    accuracy = accuracy_score(y_test, y_pred)
    precision = precision_score(y_test, y_pred)
    recall = recall_score(y_test, y_pred)
    f1 = f1_score(y_test, y_pred)
    print("Accuracy on test set: {:.2f}%".format(accuracy * 100))
    print("Precision on test set: {:.2f}%".format(precision * 100))
    print("Recall on test set: {:.2f}%".format(recall * 100))
    print("F1 score on test set: {:.2f}%".format(f1 * 100))

    # Print the confusion matrix
    cm = confusion_matrix(y_test, y_pred)
    print("Confusion matrix:\n", cm)
