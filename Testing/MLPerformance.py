import re

import nltk
from nltk.corpus import stopwords
from nltk.corpus import wordnet
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from sklearn.kernel_approximation import RBFSampler
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.naive_bayes import MultinomialNB, ComplementNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import LinearSVC

nltk.download("stopwords")
nltk.download("punkt")
nltk.download("wordnet")

stopwordEn = stopwords.words("english")
stemmer = PorterStemmer()


def lemmaWord(word):
    lemma = wordnet.morphy(word)
    if lemma is not None:
        return lemma
    else:
        return word


def stemWord(word):
    stem = stemmer.stem(word)
    if stem is not None:
        return stem
    else:
        return word


def processText(text, lemma=False, gram=1, rmStop=True, rmEmoji=True, rmPunc=True, rmFont=True):
    # remove URLs and erroneous symbols
    text = re.sub(r"(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b", "", text, flags=re.MULTILINE)

    # tokenize text and remove stop words
    tokens = word_tokenize(text)
    new_tokens = []
    stoplist = stopwordEn if rmStop else []
    for i in tokens:
        i = i.lower()
        if i.isalpha() and (i not in stoplist):
            if lemma:
                i = lemmaWord(i)
            new_tokens.append(i)

    # generate n-grams if gram > 1
    if gram <= 1:
        return new_tokens
    else:
        return [" ".join(i) for i in nltk.ngrams(new_tokens, gram)]


def getTags(text):
    token = word_tokenize(text)
    token = [i.lower() for i in token]
    train_tags = nltk.pos_tag(token)


import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score

# load the dataset
df = pd.read_excel("comments.xlsx")
df['Comment'] = df['Comment'].apply(str)

# split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(df['Comment'], df['Spam'], test_size=0.2, random_state=42)

# define the pipeline for text classification with SGD classifier and Kernel Approximation
classifiers = [('LogisticRegression', LogisticRegression()),
               ('KNN', KNeighborsClassifier()),
               ('NaiveBayes', MultinomialNB()),
               ('SVM', LinearSVC()),
               ("Adapted Naive Bayes", ComplementNB()),
               ('RandomForest', RandomForestClassifier()),
               ('SGDClassifier', SGDClassifier(loss='log', random_state=42)),
              ]
for clf_name, clf in classifiers:
    if clf_name == 'SGDClassifier':
        text_clf = Pipeline([
            ('vect', CountVectorizer(analyzer=processText)),
            ('tfidf', TfidfTransformer(use_idf=True)),
            ('rbf', RBFSampler(gamma=1, random_state=42)),
            ("clf", clf),
        ])
    else:
        text_clf = Pipeline([
            ('vect', CountVectorizer(analyzer=processText)),
            ('tfidf', TfidfTransformer(use_idf=True)),
            ("clf", clf),
        ])
    print(f'Training {clf_name}...')
    text_clf.fit(X_train, y_train)

    # evaluate the model on the testing set
    y_pred = text_clf.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    precision = precision_score(y_test, y_pred)
    recall = recall_score(y_test, y_pred)
    f1 = f1_score(y_test, y_pred)
    print(f"Results for {clf_name}:")
    print("Accuracy on test set: {:.2f}%".format(accuracy * 100))
    print("Precision on test set: {:.2f}%".format(precision * 100))
    print("Recall on test set: {:.2f}%".format(recall * 100))
    print("F1 score on test set: {:.2f}%".format(f1 * 100))
    # print the confusion matrix
    cm = confusion_matrix(y_test, y_pred)
    print("Confusion matrix:\n", cm)
